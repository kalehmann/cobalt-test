FROM rust:stretch

RUN git clone https://github.com/cobalt-org/cobalt.rs.git \
    && cd cobalt.rs \
    && PATH="$HOME/.cargo/bin:$PATH" cargo build --release --features=sass

FROM debian:stretch-slim
COPY --from=0 /cobalt.rs/target/release/cobalt /usr/bin/cobalt